package fusefs

import (
	"bitbucket.org/chtisgit/googledrive/cache"
	"github.com/hanwen/go-fuse/fuse"
	"github.com/hanwen/go-fuse/fuse/nodefs"
	"github.com/hanwen/go-fuse/fuse/pathfs"
	"github.com/sirupsen/logrus"
)

type T struct {
	pathfs.FileSystem
	cache *cache.T
}

func New(mountpoint string, cache *cache.T) (*fuse.Server, error) {
	nfs := pathfs.NewPathNodeFs(&T{
		FileSystem: pathfs.NewDefaultFileSystem(),
		cache:      cache,
	}, nil)

	s, _, err := nodefs.MountRoot(mountpoint, nfs.Root(), nil)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (f *T) GetAttr(path string, context *fuse.Context) (*fuse.Attr, fuse.Status) {
	logrus.WithField("path", path).Info("getattr")
	a, err := f.cache.GetAttributes(path)
	if err != nil {
		return nil, fuse.ENOENT
	}
	return a, fuse.OK
}

func (f *T) OpenDir(path string, context *fuse.Context) ([]fuse.DirEntry, fuse.Status) {
	logrus.WithField("path", path).Info("opendir")
	d, err := f.cache.GetDirectory(path)
	if err != nil {
		logrus.WithError(err).WithField("path", path).Error("opendir")
		return nil, fuse.ENOENT
	}
	return d, fuse.OK
}

func (f *T) Open(path string, flags uint32, context *fuse.Context) (nodefs.File, fuse.Status) {
	logrus.WithField("path", path).Info("open")
	file := f.cache.GetFile(path)
	if file == nil {
		logrus.WithField("path", path).Error("GetFile returned nil")
		return nil, fuse.EINVAL
	}
	logrus.WithField("path", path).Info("open returning file handle")
	return file, fuse.OK
}
