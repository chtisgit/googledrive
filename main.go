package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/chtisgit/googledrive/auth"
	"bitbucket.org/chtisgit/googledrive/cache"
	"bitbucket.org/chtisgit/googledrive/fusefs"
	"github.com/hanwen/go-fuse/fuse"
	"github.com/sirupsen/logrus"
)

func startFuse(cache *cache.T) *fuse.Server {
	path := os.Getenv("HOME") + "/GoogleDrive"
	err := os.Mkdir(path, 0600)
	if err != nil && !os.IsExist(err) {
		logrus.WithError(err).Fatalf("Unable to create %s", path)
	}
	filesys, err := fusefs.New(os.Getenv("HOME")+"/GoogleDrive", cache)
	if err != nil {
		logrus.WithError(err).Fatalf("Unable to start FUSE")
	}
	return filesys
}

func main() {
	logrus.SetLevel(logrus.InfoLevel)

	cache, err := cache.New(os.Getenv("HOME") + "/.googledrive")
	if err != nil {
		logrus.WithError(err).Fatal("Unable to set up cache")
	}
	defer cache.Close()

	logfile, err := os.Create(cache.LogFile())
	if err != nil {
		logrus.WithError(err).Error("Cannot create logfile. Logging to stderr instead.")
	} else {
		defer logfile.Close()
		logrus.SetOutput(logfile)
	}

	srv, err := auth.Authenticate(&auth.AuthFiles{
		Credential: cache.CredentialsFile(),
		Token:      cache.TokenFile(),
	})
	if err != nil {
		logrus.WithError(err).Fatal("Unable to retrieve Drive client")
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	defer func() {
		signal.Reset(syscall.SIGINT, syscall.SIGTERM)
		close(sigs)
	}()

	cacheInit := make(chan bool, 0)
	go cache.Serve(srv, cacheInit)
	<-cacheInit
	close(cacheInit)

	filesys := startFuse(cache)
	defer filesys.Unmount()
	go filesys.Serve()

	fmt.Fprintln(os.Stderr, "Ready")
	sig := <-sigs
	logrus.Infof("Received signal %v. Shutting down.", sig)
}
