package cache

import (
	"os"
	"sync"

	"github.com/boltdb/bolt"
	"github.com/sirupsen/logrus"
	drive "google.golang.org/api/drive/v3"
)

type T struct {
	dir    string
	db     *bolt.DB
	rootID string
	fcache map[string]*File
	flock  sync.Mutex
	srv    *drive.Service
}

func New(dir string) (*T, error) {
	err := os.Mkdir(dir, 0700)
	if err == nil {

	} else if !os.IsExist(err) {
		return nil, err
	}
	c := &T{
		dir:    dir,
		fcache: map[string]*File{},
	}
	_ = os.Mkdir(dir+c.CacheFile([]byte{}), 0755)
	return c, nil
}

func (c *T) Close() {
	if c.db != nil {
		c.db.Close()
	}

	c.flock.Lock()
	defer c.flock.Unlock()
	for key, file := range c.fcache {
		file.Close()
		delete(c.fcache, key)
	}
}

func (c *T) CacheFile(id []byte) string {
	return c.dir + "/cache/" + string(id)
}

func (c *T) LogFile() string {
	return c.dir + "/log.txt"
}

func (c *T) CredentialsFile() string {
	return c.dir + "/credentials.json"
}

func (c *T) TokenFile() string {
	return c.dir + "/token.json"
}

func (c *T) DBFile() string {
	return c.dir + "/meta.db"
}

func isParent(par string, file *drive.File) bool {
	if file.Parents == nil || par == file.Id {
		return false
	}
	for i := range file.Parents {
		if par == file.Parents[i] {
			return true
		}
	}
	return false
}

func isFolder(file *drive.File) bool {
	return file.MimeType == "application/vnd.google-apps.folder"
}

func (c *T) Serve(srv *drive.Service, initDone chan<- bool) error {
	c.srv = srv
	_ = c.srv.Changes.GetStartPageToken()

	err := c.initDB(srv)
	if err != nil {
		return err
	}

	initDone <- true
	select {}

	return nil
}

func (c *T) download(id []byte) *File {
	strid := string(id)

	// take the lock of the file and load its contents in a different go-routine
	// so method calls can be made before the file is completely loaded
	// they will block though
	file := &File{
		fd:        nil,
		path:      c.CacheFile(id),
		openflags: os.O_CREATE | os.O_RDWR,
	}
	err := file.Open()
	if err != nil {
		logrus.WithField("path", file.path).Warn("trying to open already open file")
	}
	file.Lock()
	go func() {
		resp, err := c.srv.Files.Get(strid).Download()
		if err != nil {
			logrus.WithField("id", strid).WithError(err).Error("Unable to download file")
			return
		}
		defer resp.Body.Close()

		file.FromReader(file.fd, resp.Body)
	}()

	c.flock.Lock()
	defer c.flock.Unlock()

	c.fcache[strid] = file

	return file
}

func (c *T) GetFile(path string) *File {
	id := c.fileID(path)
	if id != nil && len(id) > 0 {
		c.flock.Lock()
		file, found := c.fcache[string(id)]
		c.flock.Unlock()
		if found {
			return file
		}
		return c.download(id)
	}
	logrus.WithField("path", path).Error("access to invalid path")
	return nil
}
