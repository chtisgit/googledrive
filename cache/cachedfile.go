package cache

import (
	"errors"
	"io"
	"os"
	"sync"
	"time"

	"github.com/hanwen/go-fuse/fuse"
	"github.com/hanwen/go-fuse/fuse/nodefs"
)

type File struct {
	sync.Mutex
	path      string
	fd        *os.File
	openflags int
	Ctime     time.Time
	MTime     time.Time
	ATime     time.Time

	nodefs.File
}

func (f *File) open() error {
	var err error
	f.fd, err = os.OpenFile(f.path, f.openflags, 0644)

	return err
}

// Open opens a new file handle and returns an error
// if the file handle is not nil.
func (f *File) Open() error {
	f.Lock()
	defer f.Unlock()

	if f.fd != nil {
		return errors.New("trying to open already open file")
	}

	return f.open()
}

// OpenIfClosed opens a new file handle if it is nil. Thereafter
// it can be reopened by calling Open.
func (f *File) OpenIfClosed() error {
	f.Lock()
	defer f.Unlock()

	if f.fd != nil {
		return nil
	}

	return f.open()
}

// Close closes the file handle if it is open.
func (f *File) Close() {
	f.Lock()
	defer f.Unlock()

	if f.fd != nil {
		f.fd.Close()
		f.fd = nil
	}
}

// FromReader loads the contents of the file from a Reader.
// be sure to call Lock() before calling this method.
func (f *File) FromReader(fd *os.File, r io.Reader) {
	defer f.Unlock()

	io.Copy(fd, r)
	fd.Seek(0, os.SEEK_SET)

	f.ATime = time.Now()
	f.MTime = f.ATime
}

// SetInode idk.
func (f *File) SetInode(i *nodefs.Inode) {
}

// String returns the actual path of the file.
func (f *File) String() string {
	return f.path
}

// InnerFile returns the receiver as nodefs.File interface.
func (f *File) InnerFile() nodefs.File {
	return f
}

func (f *File) Read(dest []byte, off int64) (fuse.ReadResult, fuse.Status) {
	f.Lock()
	defer f.Unlock()

	rd, err := f.fd.ReadAt(dest, off)

	if err != nil {
		if err == io.EOF {
			return fuse.ReadResultData(dest[:rd]), fuse.OK
		}
		return nil, fuse.ToStatus(err)
	}

	f.ATime = time.Now()
	return fuse.ReadResultData(dest), fuse.OK
}
func (f *File) Write(data []byte, off int64) (written uint32, code fuse.Status) {
	f.Lock()
	defer f.Unlock()

	wr, err := f.fd.WriteAt(data, off)

	if err != nil {
		return 0, fuse.ToStatus(err)
	}

	f.ATime = time.Now()
	f.MTime = f.ATime
	return uint32(wr), fuse.OK
}

func (f *File) GetLk(owner uint64, lk *fuse.FileLock, flags uint32, out *fuse.FileLock) (code fuse.Status) {
	return fuse.ENOSYS
}
func (f *File) SetLk(owner uint64, lk *fuse.FileLock, flags uint32) (code fuse.Status) {
	return fuse.ENOSYS
}
func (f *File) SetLkw(owner uint64, lk *fuse.FileLock, flags uint32) (code fuse.Status) {
	return fuse.ENOSYS
}

// Flush is called for close() call on a file descriptor. In
// case of duplicated descriptor, it may be called more than
// once for a file.
func (f *File) Flush() fuse.Status {
	return fuse.OK
}

// This is called to before the file handle is forgotten. This
// method has no return value, so nothing can synchronizes on
// the call. Any cleanup that requires specific synchronization or
// could fail with I/O errors should happen in Flush instead.
func (f *File) Release() {
	f.Lock()
	defer f.Unlock()

	f.Close()
}
func (f *File) Fsync(flags int) (code fuse.Status) {
	if err := f.fd.Sync(); err != nil {
		return fuse.EIO
	}
	return fuse.OK
}

// This comment is copied from the fuse module:
// The methods below may be called on closed files, due to
// concurrency.  In that case, you should return EBADF.

// Truncate sets the file size of the file. Currently not implemented.
func (f *File) Truncate(size uint64) fuse.Status {
	f.Lock()
	defer f.Unlock()

	if f.fd == nil {
		return fuse.EBADF
	}

	sz := int64(size)
	if sz < 0 {
		return fuse.EINVAL
	}

	err := f.fd.Truncate(sz)

	f.ATime = time.Now()
	f.MTime = f.ATime
	return fuse.ToStatus(err)
}
func (f *File) GetAttr(out *fuse.Attr) fuse.Status {
	f.Lock()
	defer f.Unlock()

	if f.fd == nil {
		return fuse.EBADF
	}

	return fuse.ENOSYS
}
func (f *File) Chown(uid uint32, gid uint32) fuse.Status {
	f.Lock()
	defer f.Unlock()

	if f.fd == nil {
		return fuse.EBADF
	}

	return fuse.EPERM
}
func (f *File) Chmod(perms uint32) fuse.Status {
	f.Lock()
	defer f.Unlock()

	if f.fd == nil {
		return fuse.EBADF
	}

	return fuse.EPERM
}
func (f *File) Utimens(atime *time.Time, mtime *time.Time) fuse.Status {
	f.Lock()
	defer f.Unlock()

	if f.fd == nil {
		return fuse.EBADF
	}

	return fuse.ENOSYS
}
func (f *File) Allocate(off uint64, size uint64, mode uint32) (code fuse.Status) {
	f.Lock()
	defer f.Unlock()

	if f.fd == nil {
		return fuse.EBADF
	}

	return fuse.ENOSYS
}
