package cache

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/boltdb/bolt"
	"github.com/hanwen/go-fuse/fuse"
	"github.com/sirupsen/logrus"
	drive "google.golang.org/api/drive/v3"
)

func (c *T) addFiles(files []*drive.File) {
	c.db.Update(func(tx *bolt.Tx) error {
		fileb, err := tx.CreateBucketIfNotExists([]byte("filedb"))
		if err != nil {
			logrus.WithError(err).Fatal("DB error")
		}

		for _, i := range files {
			j, err := i.MarshalJSON()
			if err != nil {
				logrus.WithField("name", i.Name).WithError(err).Error("Cannot add file to DB")
				continue
			}

			err = fileb.Put([]byte(i.Id), j)
			if err != nil {
				logrus.WithError(err).Fatal("DB error")
			}
		}
		return nil
	})
}

func searchFileDB(filedb *bolt.Bucket, paths *bolt.Bucket, prefix string, id string) {
	if len(prefix) > 1024 {
		return
	}

	cs := filedb.Cursor()
	for k, v := cs.First(); k != nil; k, v = cs.Next() {
		file := drive.File{}
		_ = json.Unmarshal(v, &file)

		if isParent(id, &file) {
			path := prefix + "/" + file.Name
			paths.Put([]byte(path), []byte(file.Id))
			if isFolder(&file) {
				searchFileDB(filedb, paths, path, file.Id)
			}
		}
	}

}

func (c *T) buildPathsDB() {
	c.db.Update(func(tx *bolt.Tx) error {
		filedb := tx.Bucket([]byte("filedb"))
		logrus.Info("Create bucket paths")
		paths, err := tx.CreateBucketIfNotExists([]byte("paths"))
		if err != nil {
			logrus.WithError(err).Fatal("DB error")
		}
		paths.Put([]byte("/"), []byte(c.rootID))
		searchFileDB(filedb, paths, "", c.rootID)
		return nil

	})
}

func (c *T) populateDB(srv *drive.Service) {
	const FilesPerRequest = 300

	root, err := srv.Files.Get("root").Fields("id,name,mimeType").Do()
	if err != nil {
		logrus.WithError(err).Fatal("Cannot retrieve GDrive root directory")
	}
	c.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("filedb"))
		if err != nil {
			logrus.WithError(err).Fatal("DB error")
		}

		j, err := root.MarshalJSON()
		if err != nil {
			logrus.WithError(err).Fatal("Cannot get root node")
		}

		c.rootID = root.Id
		err = b.Put([]byte(root.Id), j)
		if err != nil {
			logrus.WithError(err).Fatal("DB error")
		}

		return nil
	})

	pagetoken := ""
	count := 0
	for {
		list := srv.Files.List().PageSize(FilesPerRequest).OrderBy("folder,modifiedTime,name")
		if pagetoken != "" {
			list = list.PageToken(pagetoken)
		}

		r, err := list.Fields("nextPageToken, files(id, name, mimeType, size, parents)").Do()
		if err != nil {
			logrus.WithError(err).Fatal("Unable to retrieve files")
		}
		pagetoken = r.NextPageToken
		c.addFiles(r.Files)
		count += len(r.Files)

		if len(r.Files) < FilesPerRequest || pagetoken == "" {
			break
		}
	}
	fmt.Fprintf(os.Stderr, "Got %d files.\n", count)
}

func (c *T) initDB(srv *drive.Service) error {
	var err error
	c.db, err = bolt.Open(c.DBFile(), 0600, nil)
	if err != nil {
		return err
	}

	initialized := false
	c.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("paths"))
		if b != nil {
			initialized = true
		}
		return nil
	})
	if initialized {
		return nil
	}

	c.populateDB(srv)
	c.buildPathsDB()

	return nil
}

func (c *T) fileID(path string) []byte {
	var id []byte
	c.db.View(func(tx *bolt.Tx) error {
		paths := tx.Bucket([]byte("paths"))
		id = paths.Get([]byte("/" + path))
		return nil
	})

	return id

}

func (c *T) fileMeta(id []byte) *drive.File {
	file := &drive.File{}
	c.db.View(func(tx *bolt.Tx) error {
		filedb := tx.Bucket([]byte("filedb"))
		j := filedb.Get(id)
		err := json.Unmarshal(j, file)
		if err != nil {
			logrus.WithError(err).WithField("json", j).Error("fileMeta")
			file = nil
		}
		return nil
	})

	return file
}

func (c *T) GetDirectory(path string) ([]fuse.DirEntry, error) {
	if strings.Index(path, "/*/") != -1 {
		return nil, errors.New("weird xdg-open query")
	}

	var err error
	d := []fuse.DirEntry{}

	c.db.View(func(tx *bolt.Tx) error {
		paths := tx.Bucket([]byte("paths"))
		prefix := []byte("/")

		if path != "" {
			prefix = []byte("/" + path + "/")
		}

		if id := paths.Get([]byte("/" + path)); id == nil {
			err = errors.New("no such directory")
			return nil
		}

		cs := paths.Cursor()

		for k, v := cs.Seek(prefix); k != nil && bytes.HasPrefix(k, prefix); k, v = cs.Next() {
			if bytes.Equal(k, prefix) {
				continue
			}
			name := k[len(prefix):]
			if bytes.IndexByte(name, '/') != -1 {
				continue
			}
			meta := c.fileMeta(v)
			if meta == nil {
				logrus.WithFields(
					logrus.Fields{
						"path": string(k),
						"id":   string(v),
					}).Error("not found")
				continue
			}
			mode := uint32(fuse.S_IFREG)
			if isFolder(meta) {
				mode = uint32(fuse.S_IFDIR)
			}
			d = append(d, fuse.DirEntry{
				Name: string(name),
				Mode: mode,
			})
		}

		return nil
	})

	return d, err
}

func (c *T) GetAttributes(path string) (*fuse.Attr, error) {
	var err error
	a := &fuse.Attr{
		Mode: fuse.S_IFREG | 0644,
	}
	c.db.View(func(tx *bolt.Tx) error {
		paths := tx.Bucket([]byte("paths"))
		id := paths.Get([]byte("/" + path))

		if string(id) == c.rootID {
			a.Mode = fuse.S_IFDIR | 0700
			return nil
		}

		meta := c.fileMeta(id)
		if meta == nil {
			err = os.ErrNotExist
			logrus.WithError(err).WithFields(
				logrus.Fields{
					"id":      string(id),
					"path":    "/" + path,
					"pathlen": len("/" + path),
				}).Error("not found")
			return nil
		}

		if isFolder(meta) {
			a.Mode = fuse.S_IFDIR | 0755
		} else {
			a.Size = uint64(meta.Size)
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return a, nil
}
